import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import os
import csv


# the questionnaire has 21 questions.
# all have 4 possible answers except 16 and 18 which have 7.


CURRENT_PATH = "/Users/pegah_abed/Documents/"
TASK_3_DATA_PATH = os.path.join(CURRENT_PATH,"ERISK/data/T3_data/organized")

NUMBER_OF_QUESTIONS = 21


# If CENTER == True, then each user's feature vector would be the average of features of all their sentences.
# We compare this average feature vector with feature vector of each answer to pick the most similar answer

# If CENTER == False, then we compare eah answer's feature vector with every senetnce feautre vector of the user.
# In other words, we are trying to see if the user has ever said 'any' sentence that would be similar to one of the anwers.

CENTER = True
ALL = False
CHUNK = False
if CHUNK:
    NUMBER_OF_CHUNKS = 100



def get_BDI_features(source):
    """
    This function uses the source parameter to decide which BDI feature csv file to read.
    It returns a new dataframe containing only the feature columns.

    :param source: Indicates the tool that has been used to generate the features.
    :return: Dataframe. Selects and returns only the columns that correspond to feature values.
    """

    file_name = 'BDI_q_' + source + "_features.csv"

    BDI_features = None

    if source == 'GPT':
        BDI_features_df = pd.read_csv(os.path.join(TASK_3_DATA_PATH, file_name))

        if len(BDI_features_df) !=90 :
            print ("BDI features dataframe should have 90 rows. You have {} rows.".format(len(BDI_features_df)))

        cols = [c for c in BDI_features_df.columns if 'GPT' in c] # pick the columns with 'GPT' in their name.

        if len(cols) != 768:
            print ("Something is wrong. You are supposed to get 768 features from GPT. You have {}".format(len(cols)))

        BDI_features = BDI_features_df[cols]


    elif source == 'universal':
        BDI_features_df = pd.read_csv(os.path.join(TASK_3_DATA_PATH, file_name))

        if len(BDI_features_df) != 90:
            print ("BDI features dataframe should have 90 rows. You have {} rows.".format(len(BDI_features_df)))

        cols = [c for c in BDI_features_df.columns if
                'universal' in c]  # pick the columns with 'universal' in their name.

        if len(cols) != 512:
            print (
                "Something is wrong. You are supposed to get 512 features from Universal Sentences. You have {}".format(
                    len(cols)))

        BDI_features = BDI_features_df[cols]


    return BDI_features



def generate_answers_for_user(BDI_features, user_features):
    """
    Creates a list of this user's answers to the BDI questions.

    :param BDI_features: Dataframe of the features of BDI questions.
    Each row corresponds to an answer from the questions.
    :param user_features:  Vector of this user's features form their posts.
    :return: a list of this user's answers to the BDI questions.
    """

    answers_list = []
    start = 0
    for i in range(NUMBER_OF_QUESTIONS):

        # Goes through each question,
        # from BDI features, selects the ones that belong to this question's answers and appends them to a list

        this_question_answers_feature_list = []

        if i == 15 or i == 17: # Questions 16 and 18 have 7 possible answers.
        # Since the counter starts from 0 instead of 1, the counter values to check the question number will be 15 and 17 insteqd of 16 and 18.
            length = 7

        else:
            length = 4 # other questions have 4 possible answers

        for j in range(start, start + length):

            this_question_answers_feature_list.append(list(BDI_features.iloc[j]))

        if start > len(BDI_features) - length:
            break

        start += length

        if CENTER :
            # average over all user sentences
            answers_list.append(pick_answer_for_this_question(this_question_answers_feature_list, user_features))

        else: # if ALL or CHUNK
            # Depending on the user_features parameter,
            # go through every user sentence separately or go through every chunk of the user's sentences
            answers_list.append(pick_answer_for_this_question_across_all_or_chunks(this_question_answers_feature_list, user_features))

    return answers_list



def pick_answer_for_this_question(answers_feature_list, user_features):

    """
    Computes the cosine similarity between the user's previous content and each answer of the question and picks
    the answer that gives the highest cosine similarity value.

    :param answers_feature_list: A list of lists. The number of inner lists equals to the number of answers the questions has.
    :param user_features: Features of a particular user's previous content.
    :return: An integer indicating the answer for a particular question
    """

    max  = 0  # to check which answer gives the highest cosine similarity value
    pick = 0 # indicates which answer to pick

    # cosine similarity expects 2D vectors. So we have tor eshape the 1D vectors into 2D.
    user_features_array = user_features.values.reshape(1, -1)

    for i in range(len(answers_feature_list)):

        answer_array = np.asarray(answers_feature_list[i]).reshape(1, -1)
        cosine_sim_res = cosine_similarity(user_features_array, answer_array)

        if cosine_sim_res > max:
            max = cosine_sim_res
            pick = i

    return pick


def pick_answer_for_this_question_across_all_or_chunks(answers_feature_list, user_features_list):

    """
    Depending on the values in user_features_list:

        Computes the cosine similarity between each of the user's sentences and each answer of the question and picks
        the answer that gives the highest cosine similarity value with any of the sentences.

    OR
         Computes the cosine similarity between each of the user's feature vectors (each feature vector corresponds to an
          aggregation of a chunk of the user sentences) and each answer of the question and picks
        the answer that gives the highest cosine similarity value with any of the user feature vectors.


    :param answers_feature_list: A list of lists. The number of inner lists equals to the number of answers the questions has.
    :param user_features: A list of sentence feature vectors of this user
                          OR
                          a list of feature vectors that correspond to a chunk of user's sentences.
    :return: An integer indicating the answer for a particular question
    """

    max_list = [0] * len(answers_feature_list)

    for i in range(len(answers_feature_list)): # I am comparing each answer of this question
        for sentence in user_features_list: # to every sentence or chunk of sentences of this user

            sentence_array = np.array(sentence).reshape(1, -1) # cosine similarity function requires 2D array.
            answer_array = np.asarray(answers_feature_list[i]).reshape(1, -1) # cosine similarity function requires 2D array.

            cosine_sim_res = cosine_similarity(answer_array, sentence_array)

            # For this particular answer, which sentence has the highest cosine similarity value?
            if cosine_sim_res > max_list[i]:
                max_list[i] = cosine_sim_res


    pick = max_list.index(max(max_list))

    return pick




def get_user_score(answers_list):

    """
    Calculates the user's score based on their answers to the BDI questions.

    :param answers_list: A list of integers that indicates the user's answers to the BDI questionnaire.
    :return: An integer between 0 and 63 (inclusive) indicating the user's score from the BDI questionnaire.
    """

    # Questions 16 and 18 have a different format for scores.
    # Possible answers are:
    # 0
    # 1a
    # 1b
    # 2a
    # 2b
    # 3a
    # 3b

    # Since the list indices start from 0 instead of 1, the indices to check will be 15 and 17 instead of 16 and 18.
    exceptions = [15,17]
    sum_answers = sum(answers_list) - answers_list[exceptions[0]] - answers_list[exceptions[1]]

    for exception in exceptions:
        if answers_list[exception] == 0:
            sum_answers += 0

        elif answers_list[exception] < 3:
            sum_answers += 1

        elif answers_list[exception] < 5:
            sum_answers += 2
        else:
            sum_answers += 3

    return sum_answers


def generate_predicted_answers_csv(user_features, BDI_features, output_file_name):

    """
    Creates a csv file of the predicted answers and scores. Each row corresponds to one user.
    This function should be used if CENTER == True

    :param user_features: A dataframe of all the users' features.
    :param BDI_features: A dataframe of the features of BDI questionnaire.
    :return: None.
    """

    num_users = len(user_features)
    print ("There are {} users.".format(len(user_features)))

    q_list = np.arange(1,NUMBER_OF_QUESTIONS+1) # have an array of numbers from 1 to 21
    q_list = list(map(str, q_list)) # convert the list values from int to str
    cols = ['user_id', 'score', 'category'] + q_list  # generate a list of column names

    predicted_res = pd.DataFrame(columns=cols)

    for i in range(num_users):

        # make a dict for every user to store their answers and score.
        user_dict = dict((k,[]) for k in cols)
        user_dict['user_id'] = user_features['user_id'].iloc[i]

        temp_user_features = user_features.drop(columns=['user_id'])
        answers_list = generate_answers_for_user(BDI_features, temp_user_features.iloc[i])
        user_score = get_user_score(answers_list)

        user_dict['score'] = user_score

        if 0 <= user_score <= 9:
            user_dict['category'] = 'MINIMAL'
        elif 10 <= user_score <= 18:
            user_dict['category'] = 'MILD'
        elif 19 <= user_score <= 29:
            user_dict['category'] = 'MODERATE'
        elif 30 <= user_score <= 63:
            user_dict['category'] = 'SEVERE'
        else:
            print ("The maximum possible value is 63. How did you get more?!")

        for j in range(1, NUMBER_OF_QUESTIONS+1):  # the dict key values for questions start from 1. The answers list indices starts from 0.
            str_j = str(j)
            user_dict[str_j] = answers_list[j-1]

        user_df = pd.DataFrame(user_dict, index=[0])  # convert the user dict to a dataframe
        predicted_res = predicted_res.append(user_df) # append the user's dataframe to the main dataframe

    predicted_res = predicted_res[cols]  # to preserve the order of the columns
    predicted_res.to_csv(os.path.join(TASK_3_DATA_PATH, output_file_name), index=False)



def get_users_sentence_features_as_list(all_sentences, col_name):
    """
    This function groups the feature sentences that belong to each user as a list and stores them into a new csv file.

    :param all_sentences: csv file of all the sentences of all the the users as input

    :param col_name: The str that has been used in the feature columns. It is 'GPT' for GPT features and 'universal' for Universal Sentences.

    :return: A csv file that has each user's sentence features as a list
    This csv file has 2 columns: 'post_id' and 'all_features'. Each value of the second column is a list of feature vectors.
    # Remember that for the EWdata, the concept of post_id is similar to user_id. Because each user has only one post.
    Each of the feature vectors corresponds to one sentence.

    Example:  If user A has k sentences, There is going to be a list of size k in the second column. Each item in this list is a feature vector.

    """

    cols = [c for c in all_sentences.columns if col_name in c]  # to get the feature columns only. Column name formats are either GPT_  or universal_sent_

    all_sentences['all_features'] = all_sentences[cols].values.tolist()
    all_sentences = all_sentences.drop(cols, axis=1)

    if ALL:
        all_sentences = all_sentences.drop(columns=['sentence_num', 'body'])

    # So far, I have a df with only 2 columns. The first column is post_id. The second column is a list of features.

    # Now I want to group the data by post-id and put the features lists that belong to the same post into a bigger list
    per_user_sentence_features_as_list = all_sentences.groupby('post_id', as_index=False).agg(lambda x: list(x))

    # The number of rows in this df should be equal to the number of users. In case of EWdata it should be 230.
    return per_user_sentence_features_as_list


def get_users_sentence_features_as_list_erisk(all_sentences, col_name):
    """
    This function groups the feature sentences that belong to each user as a list and stores them into a new csv file.

    :param all_sentences: csv file of: if ALL== True: all the sentence feature vectors of all the users as input
                                       if CHUNK == True: all the chunk feature vectors of all the users as input

    :return: A csv file that has each user's sentence features as a list
    This csv file has 2 columns: 'user_id' and 'all_features'. Each value of the second column is a list of feature vectors.
    Each of the feature vectors corresponds to one sentence.

    Example:  If user A has k sentences, There is going to be a list of size k in the second column. Each item in this list is a feature vector.

    """
    num_of_features = 0

    if col_name == 'GPT':
        num_of_features  = 768

    cols = np.arange(0, num_of_features)
    cols = list(map(str, cols))

    print ("columns are: ", cols)

    all_sentences['all_features'] = all_sentences[cols].values.tolist()
    all_sentences = all_sentences.drop(cols, axis=1)

    if ALL:
        all_sentences = all_sentences.drop(columns=['post_id','sentence_num', 'body'])

    # So far, I have a df with only 2 columns. The first column is user_id. The second column is a list of features.

    # Now I want to group the data by user-id and put the features lists that belong to the same user into a bigger list
    per_user_sentence_features_as_list = all_sentences.groupby('user_id', as_index=False).agg(lambda x: list(x))

    # The number of rows in this df should be equal to the number of users. In case of erisk it should be 20.

    print ("number of rows is: ", len(per_user_sentence_features_as_list))

    return per_user_sentence_features_as_list

def generate_predicted_answers_based_on_nearest_sentence_or_chunk(all_sentences_features, BDI_features, output_file_name, col_name):
    """
    Creates a csv file of the predicted answers and scores. Each row corresponds to one user.
    This function should be used if ALL == True or CHUNK == True

    :param user_features: A dataframe of all the users' features per sentence.
    :param BDI_features: A dataframe of the features of BDI questionnaire.
    :return: None.
    """

    if col_name == 'GPT':
        all_sentences_features = all_sentences_features.reset_index()

    if 'erisk' in output_file_name:
        per_user_sentence_features_as_list = get_users_sentence_features_as_list_erisk(all_sentences_features, col_name)
        ID_col = 'user_id'

    else:
        per_user_sentence_features_as_list = get_users_sentence_features_as_list(all_sentences_features, col_name)
        ID_col = 'post_id'

    num_users = len(per_user_sentence_features_as_list)
    print ("There are {} users.".format(num_users))

    q_list = np.arange(1,NUMBER_OF_QUESTIONS+1) # have an array of numbers from 1 to 21
    q_list = list(map(str, q_list)) # convert the list values from int to str
    cols = ['user_id', 'score', 'category'] + q_list  # generate a list of column names

    predicted_res = pd.DataFrame(columns=cols)

    for i in range(num_users):

        # make a dict for every user to store their answers and score.
        user_dict = dict((k,[]) for k in cols)


        user_dict['user_id'] = per_user_sentence_features_as_list[ID_col].iloc[i]

        temp_per_user_sentence_features_as_list = per_user_sentence_features_as_list.drop(columns=[ID_col])

        answers_list = generate_answers_for_user(BDI_features, temp_per_user_sentence_features_as_list.iloc[i]['all_features'])
        user_score = get_user_score(answers_list)

        user_dict['score'] = user_score

        if 0 <= user_score <= 9:
            user_dict['category'] = 'MINIMAL'
        elif 10 <= user_score <= 18:
            user_dict['category'] = 'MILD'
        elif 19 <= user_score <= 29:
            user_dict['category'] = 'MODERATE'
        elif 30 <= user_score <= 63:
            user_dict['category'] = 'SEVERE'
        else:
            print ("The maximum possible value is 63. How did you get more?!")

        for j in range(1, NUMBER_OF_QUESTIONS+1):  # the dict key values for questions start from 1. The answers list indices starts from 0.
            str_j = str(j)
            user_dict[str_j] = answers_list[j-1]

        user_df = pd.DataFrame(user_dict, index=[0])  # convert the user dict to a dataframe
        predicted_res = predicted_res.append(user_df) # append the user's dataframe to the main dataframe

    predicted_res = predicted_res[cols]  # to preserve the order of the columns
    predicted_res.to_csv(os.path.join(TASK_3_DATA_PATH, output_file_name), index=False)

def compare_EWdata_predicted_vs_actual_categories(predicted_file_name):
    """
    Compares the category outputs between the predicted values and the actual values.
    :return: A float indicating the number of category matches.
    """

    EWdata_predicted_res = pd.read_csv(os.path.join(TASK_3_DATA_PATH, predicted_file_name))
    EWdata_actual_res = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "EWdata_processed.csv"))

    predicted_cat = list(EWdata_predicted_res['category'])
    actual_cat = list(EWdata_actual_res['BDI_CAT'])

    match_count = 0
    for i in range(len(predicted_cat)):
        if predicted_cat[i] == actual_cat[i]:
            match_count += 1

    total = len(predicted_cat)
    match = match_count / total

    return match

def convert_final_results_csv_to_txt_file(predicted_csv_file_name, output_file_name):
    """
    Gets the user_ids and the answers to the BDI questions and stores them in a txt file.
    It also converts the answers to questions 16 and 18 back to original format (0, 1a,1b, ...)

    :param predicted_csv_file_name: A csv file of the predicted results.
    # number of rows == number of users.
    # number of columns == 24  (user_id, score, category, 21 columns for each of the BDI questions)
    :param output_file_name: The name to be used as the output_file.txt
    :return: None
    """
    predicted_csv = pd.read_csv(os.path.join(TASK_3_DATA_PATH, predicted_csv_file_name))
    user_id = list(predicted_csv['user_id'])
    cols = np.arange(1,NUMBER_OF_QUESTIONS+1) # have an array of numbers from 1 to 21
    cols = list(map(str, cols))
    answers_df = predicted_csv[cols]
    answers = answers_df.values.tolist()

    output_f = open(os.path.join(TASK_3_DATA_PATH, output_file_name), "w")

    for i in range(len(user_id)):
        output_f.write(str(user_id[i]))
        for j in range(NUMBER_OF_QUESTIONS):
            if j == 15 or j == 17:
                if answers[i][j] == 0:
                    output_f.write(' ' + '0')
                elif answers[i][j] == 1:
                    output_f.write(' ' + '1a')

                elif answers[i][j] == 2:
                    output_f.write(' ' + '1b')

                elif answers[i][j] == 3:
                    output_f.write(' ' + '2a')

                elif answers[i][j] == 4:
                    output_f.write(' ' + '2b')

                elif answers[i][j] == 5:
                    output_f.write(' ' + '3a')

                elif answers[i][j] == 6:
                    output_f.write(' ' + '3b')

            else:
                output_f.write(' ' + str(answers[i][j]))

        output_f.write("\n")


def generate_per_question_result_stat(predicted_file_name):
    """
    For each question, this function compares the predicted answer and the actual answer.
    For each question, it counts the number of users for which the predicted answer and the actual answer are the same.
    The predicted answers of questions 16 and 18 must be fixed.

    param predicted_file_name: The csv file with the predicted results.
    :return: A list that shows what percentage of predicted answers matched the actual answer for each question.
    """

    txt_file_name = predicted_file_name.split(".")[0]  # to get rid of the ".csv" part in the name
    per_question_stats_path = os.path.join(TASK_3_DATA_PATH, "per_question_stats_" + txt_file_name + ".txt")
    per_question_stats = open(per_question_stats_path, "w")

    EWdata_predicted_res = pd.read_csv(os.path.join(TASK_3_DATA_PATH, predicted_file_name))
    EWdata_actual_res = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "EWdata_processed.csv"))

    number_of_users = len(EWdata_predicted_res)

    predicted_q_cols = np.arange(1,NUMBER_OF_QUESTIONS+1)
    predicted_q_cols = list (map(str,predicted_q_cols))
    BDI_predicted_answers = EWdata_predicted_res[predicted_q_cols]


    # ----
    # For questions 16 and 18:
    # any answer value of 1 or 2 should change to 1
    # any answer value of 3 or 4 should change to 2
    # any answer value of 5 or 6 should change to 3

    BDI_predicted_answers['16'] = BDI_predicted_answers['16'].replace(to_replace=[1, 2, 3, 4, 5, 6], value=[1, 1, 2, 2, 3, 3])
    BDI_predicted_answers['18'] = BDI_predicted_answers['18'].replace(to_replace=[1, 2, 3, 4, 5, 6], value=[1, 1, 2, 2, 3, 3])

    # ----

    actual_q_cols = [""] * NUMBER_OF_QUESTIONS
    for i in range(NUMBER_OF_QUESTIONS):
        actual_q_cols[i] = 'BDI' + predicted_q_cols[i]
    BDI_actual_answers = EWdata_actual_res[actual_q_cols]


    # keeps the values that satisfy the condition and turns the other values to NaN
    compare_df = BDI_predicted_answers.where(BDI_predicted_answers.values==BDI_actual_answers.values)


    # pandas.count counts non-NA cells for each column or row. Here, we only have one column.
    # The values None, NaN, NaT, and optionally numpy.inf are considered NA
    comparison_res = round(compare_df.count() / number_of_users * 100 , 2)
    comparison_res_sorted = comparison_res.sort_values(ascending=False)

    print ("The predicted file name is: {}".format(predicted_file_name))
    print (comparison_res_sorted)

    # write the results into a txt file
    per_question_stats.write(predicted_file_name)
    per_question_stats.write("\n")
    per_question_stats.write(str(comparison_res_sorted))
    per_question_stats.write("\n")


def check_dataset_balance():
    EWdata_actual_res = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "EWdata_processed.csv"))

    number_of_users = len(EWdata_actual_res)
    minimal = EWdata_actual_res.loc[EWdata_actual_res['BDI_CAT'] == 'MINIMAL']
    mild = EWdata_actual_res.loc[EWdata_actual_res['BDI_CAT'] == 'MILD']
    moderate = EWdata_actual_res.loc[EWdata_actual_res['BDI_CAT'] == 'MODERATE']
    severe = EWdata_actual_res.loc[EWdata_actual_res['BDI_CAT'] == 'SEVERE']

    num_minimal = len(minimal)
    num_mild = len(mild)
    num_moderate = len(moderate)
    num_severe = len(severe)

    print ("minimal: {:.2f} %".format((num_minimal / number_of_users) * 100))
    print ("mild: {:.2f} %".format((num_mild / number_of_users) * 100))
    print ("moderate: {:.2f} %".format((num_moderate / number_of_users) * 100))
    print ("severe: {:.2f} %".format((num_severe / number_of_users) * 100))

    level_list = [minimal, mild, moderate, severe]
    return level_list


def generate_EWdata_actual_labels_txt_file_16_18_as_is():

    EWdata_actual_labels_path = os.path.join(TASK_3_DATA_PATH ,"EWdata_actual_lables.txt")
    EWdata_actual_res = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "EWdata_processed.csv"), index_col=None)

    EWdata_id_and_answers = EWdata_actual_res.drop(["Text", "BDI_TOTAL" , "BDI_CAT"], axis=1)
    EWdata_id_and_answers = EWdata_id_and_answers.applymap(np.int64) # to convert values to int
    EWdata_id_and_answers_path = os.path.join(TASK_3_DATA_PATH, "EWdata_actual_answers.csv")

    EWdata_id_and_answers.to_csv(EWdata_id_and_answers_path, index=False)

    with open(EWdata_actual_labels_path, "w") as my_output_file:
        with open(EWdata_id_and_answers_path, "r") as my_input_file:

            csv_reader = csv.reader(my_input_file)  # to skip the header row
            next(csv_reader)
            [my_output_file.write(" ".join(row) + '\n') for row in csv_reader]

        my_output_file.close()





if __name__ == '__main__':


    feature_sources = ['GPT', 'universal']

    # ---- Pick you feature source and input file name ----
    source = feature_sources[0]

    # input file name can be either 'erisk' or 'EWdata'
    input_file_name =  "erisk"

    check_dataset_balance()

    # ---- Get the user features and BDI features ----

    user_file = source + "_" + input_file_name + "_user_features.csv"
    user_features = pd.read_csv(os.path.join(TASK_3_DATA_PATH, user_file))
    BDI_features = get_BDI_features(source)


    # ---- Call a function to predict answers. The choice of function depends on the value of CENTER ----

    if CENTER == True:
        predicted_answers_file_name = source + "_" + input_file_name + "_predicted_answers.csv"
        generate_predicted_answers_csv(user_features, BDI_features, predicted_answers_file_name)

    elif ALL == True:
        predicted_answers_file_name = source + "_" + input_file_name + "_predicted_answers_nearest.csv"
        all_sentences_file = source + "_" + input_file_name + "_sentence_features.csv"
        all_sentences =  pd.read_csv(os.path.join(TASK_3_DATA_PATH, all_sentences_file), index_col=0)
        generate_predicted_answers_based_on_nearest_sentence_or_chunk(all_sentences, BDI_features, predicted_answers_file_name, source)


    elif CHUNK == True:
        predicted_answers_file_name = source + "_" + input_file_name + "_predicted_answers_" + str(NUMBER_OF_CHUNKS) +"_chunks.csv"
        sentences_in_chunks_file = source + "_" + input_file_name + "_sentences_in_" + str(NUMBER_OF_CHUNKS)+ "_chunks.csv"
        sentences_in_chunks_features = pd.read_csv(os.path.join(TASK_3_DATA_PATH, sentences_in_chunks_file), index_col=0)
        generate_predicted_answers_based_on_nearest_sentence_or_chunk(sentences_in_chunks_features, BDI_features,
                                                                predicted_answers_file_name, source)


    # ---- Compare your predicted results with actual labels ----

    if input_file_name != "erisk":
        match = compare_EWdata_predicted_vs_actual_categories( )
        print ("There is a {:.2f} % match.".format(match*100))

        # ---- Generate some stats on how well the model worked on different questions ----
        generate_per_question_result_stat(predicted_answers_file_name)


    # ---- Convert the resuls into a txt file ----

    if CENTER:
        output_txt_file_name = source + "_" + input_file_name +"_results.txt"
    elif ALL:
        output_txt_file_name = source + "_" + input_file_name + "_results_nearest.txt"
    else:  #CHUNK
        output_txt_file_name = source + "_" + input_file_name + "_results_" + str(NUMBER_OF_CHUNKS) + "_chunks.txt"



    convert_final_results_csv_to_txt_file(predicted_answers_file_name, output_txt_file_name)














