import pandas as pd
import numpy as np
import similarity
import os
import csv
from scipy import stats
import random
#np.random.seed(12345678)
np.random.seed(2)

# the questionnaire has 21 questions.
# all have 4 possible answers except 16 and 18 which have 7.


CURRENT_PATH = "/Users/pegah_abed/Documents/"
TASK_3_DATA_PATH = os.path.join(CURRENT_PATH,"ERISK/data/T3_data/organized")

NUMBER_OF_QUESTIONS = 21


ERISK = True

if ERISK:
    NUMBER_OF_SUBJECTS = 20
else:
    NUMBER_OF_SUBJECTS = 230



def add_score_and_category(file_name):

    """
    This function reads the csv file of the results from autosklearn.
    It then calculates each user's score and category based on their answers to the BDI questions and puts them into two new columns.
    The function saves the new dataframe into a new csv file.

    :param file_name: String. The file name of the results from supervised learning. (autosklearn results)
    :return: None
    """

    processed_file_name = file_name.split(".csv")[0] + "_processed.csv"

    predictions = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "predictions", file_name), index_col=0)

    predicted_q_cols = np.arange(1, NUMBER_OF_QUESTIONS + 1)
    predicted_q_cols = list(map(str, predicted_q_cols))

    for i in range(NUMBER_OF_QUESTIONS):
        predicted_q_cols[i] = 'BDI' + predicted_q_cols[i]

    score_list = []
    cat_list = []
    for index_of_row, row in predictions.iterrows():

        row_answers = list(row[predicted_q_cols])
        user_score = similarity.get_user_score(row_answers)
        score_list.append(user_score)


        if 0 <= user_score <= 9:
            cat_list.append('MINIMAL')
        elif 10 <= user_score <= 18:
            cat_list.append('MILD')
        elif 19 <= user_score <= 29:
            cat_list.append('MODERATE')
        elif 30 <= user_score <= 63:
            cat_list.append('SEVERE')

    predictions['score'] = score_list
    predictions['category'] = cat_list


    predictions = predictions.sort_values('user_id').reset_index(drop=True)
    predictions.to_csv(os.path.join(TASK_3_DATA_PATH, "predictions", processed_file_name))

    return predictions


def compare_category_on_holdout(predictions_csv_file, actual_csv_file):

    """
    This functions returns the percentage of users for whom the predicted category and the actual directory are the same.

    :param predictions_csv_file: csv file that has each user's predictions of the BDI answers, the score, and the category.
    :param actual_csv_file: The csv file that has the actual answers to the BDI questions, the scores and the category for each user.
    :return: float.
    """

    predictions = predictions_csv_file.rename(columns={"user_id": "ID", "category": "predicted_category", "score": "predicted_score"})

    holdout_res = pd.merge(predictions[['ID', 'predicted_category', 'predicted_score']], actual_csv_file[['ID', 'BDI_TOTAL', 'BDI_CAT']], on =['ID'] , how='left')

    # select the rows for which the predicted category and the actual category are the same
    category_match = holdout_res.loc[holdout_res['BDI_CAT'] == holdout_res['predicted_category']]

    match = round((len(category_match) / len(holdout_res))*100 , 2)

    return match



def compare_per_question_and_category_on_holdout(predictions_csv_file_name, actual_csv_file):
    """
    This functions compares the predicted answers and categories with the corresponding actual values and computes the percetage of users for whom there is a match.

    :param predictions_csv_file: csv file that has each user's predictions of the BDI answers, the score, and the category.
    :param actual_csv_file: The csv file that has the actual answers to the BDI questions, the scores and the category for each user.
    :return: float.
    """

    txt_file_name = predictions_csv_file_name.split(".")[0]  # to get rid of the ".csv" part in the name
    per_question_stats_path = os.path.join(TASK_3_DATA_PATH, "predictions", "per_question_and_category_stats_" + txt_file_name + ".txt")
    stats_file = open(per_question_stats_path, "w")

    predictions_csv_file = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "predictions", predictions_csv_file_name))
    predictions_csv_file = predictions_csv_file.sort_values('user_id')

    holdout_IDs = list(predictions_csv_file['user_id'])
    actual_csv_file = actual_csv_file[actual_csv_file['ID'].isin(holdout_IDs)]
    actual_csv_file = actual_csv_file.sort_values('ID')
    actual_csv_file.to_csv(os.path.join(TASK_3_DATA_PATH, "holdout_actual.csv"))


    number_of_users = len(predictions_csv_file)

    cols = np.arange(1,NUMBER_OF_QUESTIONS+1)
    cols = list (map(str,cols))


    actual_q_cols = [""] * NUMBER_OF_QUESTIONS
    for i in range(NUMBER_OF_QUESTIONS):
        actual_q_cols[i] = 'BDI' + cols[i]

    BDI_actual_answers = actual_csv_file[actual_q_cols]
    BDI_predicted_answers = predictions_csv_file[actual_q_cols]

    # keeps the values that satisfy the condition and turns the other values to NaN
    compare_df = BDI_predicted_answers.where(BDI_predicted_answers.values==BDI_actual_answers.values)


    # pandas.count counts non-NA cells for each column or row. Here, we only have one column.
    # The values None, NaN, NaT, and optionally numpy.inf are considered NA
    comparison_res = round(compare_df.count() / number_of_users * 100 , 2)
    comparison_res_sorted = comparison_res.sort_values(ascending=False)

    print (comparison_res_sorted)


    # write the results into a txt filee)
    stats_file.write("answers match: ")
    stats_file.write("\n")
    stats_file.write(str(comparison_res_sorted))
    stats_file.write("\n")
    stats_file.write("category match: ")
    stats_file.write("\n")
    stats_file.write(str(compare_category_on_holdout(predictions_csv_file, actual_csv_file)))
    stats_file.write("\n")

    stats_file.close()


def convert_tab_delimiter_to_space(path_to_file):

    new_path_to_file = path_to_file.split(".")[0] + "_s.txt"

    with open(path_to_file) as infile:
        with open(new_path_to_file, 'w') as outfile:
            for line in infile:
                fields = line.split('\t')
                outfile.write(' '.join(fields))

    return new_path_to_file



def convert_txt_to_csv_and_process(path_to_txt_file):
    """
    Converts the txt file into csv file and adds total score and category columns.
    :param path_to_txt_file: str. the path to the txt file
    :return: str. the path to the csv file
    """

    path_to_csv_file =  path_to_txt_file.split(".")[0] + ".csv"  # using the .txt path to generate .csv path

    # Writes the rows from the txt file into a csv file
    txt_file = open(path_to_txt_file, "r")
    csv_file = open(path_to_csv_file, "w")
    in_txt = csv.reader(txt_file, delimiter=' ')
    out_csv = csv.writer(csv_file)
    out_csv.writerows(in_txt)
    csv_file.close()


    # Adding columns
    q_numbers = np.arange(1, NUMBER_OF_QUESTIONS+1)
    q_numbers = [str(x) for x in q_numbers]

    names = ["ID"] + q_numbers # creating column titles

    csv_file = pd.read_csv(path_to_csv_file, header=None)
    csv_file = csv_file.iloc[:, 0:NUMBER_OF_QUESTIONS+1]
    csv_file.columns = names
    csv_file = csv_file.sort_values(by=['ID'])
    csv_file['ID'] = csv_file['ID'].apply(lambda x: str(x))  # making sure the ID column has type string

    total_col = list(csv_file.sum(axis=1))  # this will sum the int values in the rows.
    cat_col = []
    cat_int_col = []

    for i in range(csv_file.shape[0]):
        if ERISK: # in the erisk data, the answers to questions 16 and 18 contain alphabet. We want to extract the int part
            total_col[i] += int(''.join(x for x in csv_file.iloc[i, 16] if x.isdigit()))
            total_col[i] += int(''.join(x for x in csv_file.iloc[i, 18] if x.isdigit()))

        if 0 <= total_col[i] <= 9:
            cat_col.append('MINIMAL')
            cat_int_col.append(0)
        elif 10 <= total_col[i] <= 18:
            cat_col.append('MILD')
            cat_int_col.append(1)
        elif 19 <= total_col[i] <= 29:
            cat_col.append('MODERATE')
            cat_int_col.append(2)
        elif 30 <= total_col[i] <= 63:
            cat_col.append('SEVERE')
            cat_int_col.append(3)

    csv_file['total'] = total_col
    csv_file['category'] = cat_col
    csv_file['category_int'] = cat_int_col

    csv_file.to_csv(path_to_csv_file, index=None)

    return path_to_csv_file



def AHR(ground_truth_file, predictions_file):
    """
    Calculates the Average Hit Rate.
    The ratio of cases where the predicted answer exactly matches the actual answer.
    Averaged over all users.

    :param ground_truth_file: csv. actual answers
    :param predictions_file: csv. predicted answers
    :return: float. the AHR value
    """

    number_of_subjects = ground_truth_file.shape[0]
    total_count = NUMBER_OF_QUESTIONS * number_of_subjects

    # count the number of exact matches in the questions
    match_count = 0
    prediction_higher = 0
    prediction_lower = 0

    for row_num in range(number_of_subjects):
        for col_num in range(1, NUMBER_OF_QUESTIONS + 1):
            if ground_truth_file.iloc[row_num, col_num] == predictions_file.iloc[row_num, col_num]:
                match_count += 1

            else:

                g_str = str(ground_truth_file.iloc[row_num, col_num])
                p_str = str(predictions_file.iloc[row_num, col_num])

                g = int(''.join(x for x in g_str if x.isdigit()))
                p = int(''.join(x for x in p_str if x.isdigit()))

                if g > p:
                    prediction_lower += 1
                elif g < p:
                    prediction_higher +=1

    AHR = 100 * (match_count / total_count)

    #prediction_higher = 100 * (prediction_higher / total_count)
    #prediction_lower = 100 * (prediction_lower / total_count)


    """
    print ("The number of matches is: ", match_count, " out of ", total_count)
    print ("AHR is: ", AHR)


    print ("Ratio of cases where prediction was higher: ", prediction_higher)

    print ("Ratio of cases where prediction was lower: ", prediction_lower)
    """

    return AHR


def ADODL(ground_truth_file, predictions_file):
    """
    Calculates the Average DODL.
    For each user: DODL = (63 - ad_overall)/63
    ad_overall is the absolute difference between the sum of all the actual answers and sum of all the predicted answers.
    DODL is a value between 0 and 1, inclusive. The higher the DODL value the better.

    :param ground_truth_file: csv. actual answers
    :param predictions_file: csv. predicted answers
    :return: float. the ADODL value
    """

    number_of_subjects = ground_truth_file.shape[0]


    prediction_total_higher_than_ground_truth = 0
    prediction_cat_higher_than_ground_truth = 0

    prediction_total_lower_than_ground_truth = 0
    prediction_cat_lower_than_ground_truth = 0

    prediction_higher_amount = 0
    prediction_lower_amount = 0

    prediction_total_match = 0
    prediction_cat_match = 0

    # Difference between Overall Depression Levels (looks at the total scores)
    DODL = 0
    for ID_count in range(number_of_subjects):
        if predictions_file['total'].iloc[ID_count] > ground_truth_file['total'].iloc[ID_count]:
            prediction_total_higher_than_ground_truth += 1
            difference = predictions_file['total'].iloc[ID_count] - ground_truth_file['total'].iloc[ID_count]
            prediction_higher_amount += difference
            DODL += (63 - difference) / 63

        if predictions_file['total'].iloc[ID_count] < ground_truth_file['total'].iloc[ID_count]:
            prediction_total_lower_than_ground_truth += 1
            difference = ground_truth_file['total'].iloc[ID_count] - predictions_file['total'].iloc[ID_count]
            prediction_lower_amount += difference
            DODL += (63 - difference) / 63

        if predictions_file['total'].iloc[ID_count] == ground_truth_file['total'].iloc[ID_count]:
            prediction_total_match += 1
            difference = ground_truth_file['total'].iloc[ID_count] - predictions_file['total'].iloc[ID_count]
            prediction_lower_amount += difference
            DODL += (63 - difference) / 63

        if predictions_file['category_int'].iloc[ID_count] > ground_truth_file['category_int'].iloc[ID_count]:
            prediction_cat_higher_than_ground_truth += 1

        if predictions_file['category_int'].iloc[ID_count] < ground_truth_file['category_int'].iloc[ID_count]:
            prediction_cat_lower_than_ground_truth += 1

        if predictions_file['category_int'].iloc[ID_count] == ground_truth_file['category_int'].iloc[ID_count]:
            prediction_cat_match += 1


    """
    print ("For ", prediction_total_higher_than_ground_truth,
           " of the users, predicted total was higher than actual total.")
    print (
    "On average, it is ", prediction_higher_amount / prediction_total_higher_than_ground_truth, " scores higher.")

    print (
    "For ", prediction_total_lower_than_ground_truth, " of the users, predicted total was lower than actual total.")
    print (
    "On average, it is ", prediction_lower_amount / prediction_total_lower_than_ground_truth, " scores lower.")

    print ("For ", prediction_total_match, " of the users, predicted total was equal to actual total.")

    print ("Overall, on average, the difference of scores is ",
           (prediction_lower_amount + prediction_higher_amount) / number_of_subjects)

    print ("Average DODL is: ", 100 * (DODL / number_of_subjects))

    print ("For ", prediction_cat_higher_than_ground_truth,
           " of the users, predicted category was more severe than actual category.")
    print ("For ", prediction_cat_lower_than_ground_truth,
           " of the users, predicted category was less severe than actual category.")
    print ("For ", prediction_cat_match, " of the users, predicted category was equal to actual category.")
    """



    ADODL =  100 * (DODL / number_of_subjects)
    return ADODL


def ttest(ground_truth_file, predictions_file):
    """
    ttest performed over predicted and actual total scores of all users. (two lists of size NUMBER_OF_SUBJECTS)
    :param ground_truth_file: csv. actual answers
    :param predictions_file: csv. predicted answers
    :return: ttest restuls
    """
    ground_total_list = ground_truth_file['total']
    predictions_total_list = predictions_file['total']
    t_test_res = stats.ttest_ind(ground_total_list, predictions_total_list, equal_var=False)

    print (t_test_res)
    return t_test_res




def ACR(ground_truth_file, prediction_file):
    """
    Calculates the Average Closeness Rate.
    For each question: CR = (mad-ad)/mad
    ad is the absolute difference between the predicted answer and the actual answer.
    mad is the maximum absolute difference which is 3.

    For each user, we calculate the CR values of each question and average them.
    ACR is the average of these per-user values over all the users.

    :param ground_truth_file: csv. actual answers
    :param predictions_file: csv. predicted answers
    :return: float. the ACR value
    """
    total_CR = 0
    number_of_subjects = ground_truth_file.shape[0]

    for i in range(number_of_subjects):
        this_user_CR = 0
        for j in range(1, NUMBER_OF_QUESTIONS + 1):

            if j == 16 or j == 18:
                ground_truth_file.iloc[i, j] = str(ground_truth_file.iloc[i, j])
                prediction_file.iloc[i, j] = str(prediction_file.iloc[i, j])
                g = int(''.join(x for x in ground_truth_file.iloc[i, j] if x.isdigit()))
                p = int(''.join(x for x in prediction_file.iloc[i, j] if x.isdigit()))

            else:
                g = ground_truth_file.iloc[i, j]
                p = prediction_file.iloc[i, j]


            mad = 3
            ad = abs(g - p)
            this_user_CR += (mad - ad) / mad

        this_user_CR = this_user_CR / NUMBER_OF_QUESTIONS
        total_CR += this_user_CR

    total_CR = 100 * (total_CR /number_of_subjects)

    #print (total_CR)
    return total_CR


def DCHR(ground_truth_file, predictions_file):
    """
    Calculates the Depression Category Hit Rate.
    The fraction of cases in which the predicted depression category matches the actual depression category of the subject.
    :param ground_truth_file: csv. actual answers
    :param predictions_file: csv. predicted answers
    :return: float. the DCHR value
    """
    cat_match = 0

    for i in range(NUMBER_OF_SUBJECTS):
        if ground_truth_file['category'].iloc[i] == predictions_file['category'].iloc[i]:
            cat_match +=1

    DCHR = 100 * (cat_match/NUMBER_OF_SUBJECTS)
    return DCHR


def compare_ground_truth_and_predictions(ground_truth_file, predictions_file):

    """
    Compares the ground truth answers and the predicted answers using the metric.
    :param ground_truth_file: csv. actual answers
    :param predictions_file: csv. predicted answers
    :return: tuple of 4 values.
    """

    AHR_res = AHR(ground_truth_file, predictions_file)
    ACR_res = ACR(ground_truth_file, predictions_file)
    ADODL_res = ADODL(ground_truth_file, predictions_file)
    DCHR_res = DCHR(ground_truth_file, predictions_file)

    return (AHR_res,ACR_res,ADODL_res, DCHR_res)


def compare_ground_truth_and_random_answers(ground_truth_file, random_results_path, value=None):
    """
    Generates random dataframes as answers and compared them to the ground truth.
    :param ground_truth_file: csv. actual answers
    :param random_results_path: str. The path to save the results from the random runs.
    :return: None
    """

    ID_list = []
    AHR_list = []
    ACR_list = []
    ADODL_list = []
    DCHR_list = []

    names = ['ID', 'AHR', 'ACR', 'ADODL', 'DCHR']
    random_answers_results = pd.DataFrame(columns = names)

    for i in range(1000):
        if value is None:
            random_file = generate_random_answers(ground_truth_file)
        else:
            print ("here about to generate with value = ", value)
            random_file = generate_all_same_answers(ground_truth_file, value)
        AHR_res, ACR_res, ADODL_res, DCHR_res = compare_ground_truth_and_predictions(ground_truth_file, random_file)

        ID_list.append(i)
        AHR_list.append(AHR_res)
        ACR_list.append(ACR_res)
        ADODL_list.append(ADODL_res)
        DCHR_list.append(DCHR_res)


    print ("Average AHR over 1000 runs is: ", sum(AHR_list) / 1000)
    print ("Average ACR over 1000 runs is: ", sum(ACR_list) / 1000)
    print ("Average ADODL over 1000 runs is: ", sum(ADODL_list) / 1000)
    print ("Average DCHR over 1000 runs is: ", sum(DCHR_list) / 1000)

    random_answers_results['ID'] = ID_list
    random_answers_results['AHR'] = AHR_list
    random_answers_results['ACR'] = ACR_list
    random_answers_results['ADODL'] = ADODL_list
    random_answers_results['DCHR'] = DCHR_list

    random_answers_results.to_csv(random_results_path, index=None)


def match_per_question_analysis(ground_truth_path, predictions_path):
    """
    Some question-level analysis.
    Calculates the number of exact matches per question and saves the count in a csv file.
    :param ground_truth_path: str. path to the actual answers file
    :param predictions_path: ste. path to the predicted answers file
    :return: None
    """

    ground_truth_file = pd.read_csv(ground_truth_path)
    predictions_file = pd.read_csv(predictions_path)

    number_of_subjects = ground_truth_file.shape[0]

    match_per_question_path = predictions_path.split(".")[0] + "_per_question_match_count.csv"
    match_per_question_df = pd.DataFrame(columns=["Question Number", "Match Count out of " + str(number_of_subjects)])
    q_numbers = np.arange(1, NUMBER_OF_QUESTIONS + 1)
    match_per_question_df['Question Number'] = q_numbers

    match_count_per_question_list = []
    print ("Number of subjects: ", number_of_subjects)

    for i in range(1, NUMBER_OF_QUESTIONS+1):
        print ("checking question ", i)

        ground_truth_answers = ground_truth_file.iloc[:,i]
        prediction_answers = predictions_file.iloc[:,i]

        match_count = np.count_nonzero(ground_truth_answers.values == prediction_answers.values)
        match_count_per_question_list.append(match_count)

    match_per_question_df["Match Count out of " + str(number_of_subjects)] = match_count_per_question_list

    match_per_question_df = match_per_question_df.sort_values(by=["Match Count out of " + str(number_of_subjects)], ascending=False)

    match_per_question_df.to_csv(match_per_question_path, index=None)




def generate_random_answers(ground_truth_file):
    """
    Completes the BDI questionnaire with random values.

    :param ground_truth_file: csv. used to get a list of subject IDs
    :param random_answers_path: csv. the path to save the random answers csv file
    :return: returns the randomly generated df
    """

    possible_answers = [0,1,2,3]  # list of possible answers for all questions except 16 and 18
    possible_answers_16_18 = [0, '1a', '1b', '2a', '2b', '3a', '3b'] # list of possible answers for question 16 and 18

    # Adding columns
    q_numbers = np.arange(1, NUMBER_OF_QUESTIONS + 1)
    q_numbers = [str(x) for x in q_numbers]

    names = ['ID'] + q_numbers + ['total', 'category', 'category_int' ] # creating column titles
    random_answers_df = pd.DataFrame(columns=names)

    ID_list = ground_truth_file['ID']
    random_answers_df['ID'] = ID_list


    # Generating random answers and adding the columns to the dataframe
    for i in range(1, NUMBER_OF_QUESTIONS+1):

        if i == 16 or i == 18:
            this_question = np.random.choice(possible_answers_16_18, NUMBER_OF_SUBJECTS)

        else:
            this_question = np.random.choice(possible_answers, NUMBER_OF_SUBJECTS)

        random_answers_df[str(i)] = this_question

    total_col = list(random_answers_df.sum(axis=1))  # this will sum the int values in the rows.
    cat_col = []
    cat_int_col = []

    for i in range(random_answers_df.shape[0]):

        total_col[i] += int(''.join(x for x in random_answers_df.iloc[i, 16] if x.isdigit()))
        total_col[i] += int(''.join(x for x in random_answers_df.iloc[i, 18] if x.isdigit()))

        if 0 <= total_col[i] <= 9:
            cat_col.append('MINIMAL')
            cat_int_col.append(0)
        elif 10 <= total_col[i] <= 18:
            cat_col.append('MILD')
            cat_int_col.append(1)
        elif 19 <= total_col[i] <= 29:
            cat_col.append('MODERATE')
            cat_int_col.append(2)
        elif 30 <= total_col[i] <= 63:
            cat_col.append('SEVERE')
            cat_int_col.append(3)

    random_answers_df['total'] = total_col
    random_answers_df['category'] = cat_col
    random_answers_df['category_int'] = cat_int_col


    return random_answers_df



def generate_all_same_answers(ground_truth_file, value, all_same_answers_path=None):
    """
    Completes the BDI questionnaire with all the answers chosen as zero.

    :param ground_truth_file: csv. used to get a list of subject IDs
    :param random_answers_path: csv. the path to save the 0 answers csv file
    :return: returns the generated df will all answers == 0
    """

    # Adding columns
    q_numbers = np.arange(1, NUMBER_OF_QUESTIONS + 1)
    q_numbers = [str(x) for x in q_numbers]

    names = ['ID'] + q_numbers + ['total', 'category', 'category_int']  # creating column titles
    all_same_answers_df = pd.DataFrame(columns=names)

    ID_list = ground_truth_file['ID']
    all_same_answers_df['ID'] = ID_list

    # Generating random answers and adding the columns to the dataframe

    if value == 0:
        for i in range(1, NUMBER_OF_QUESTIONS + 1):
            this_question = [0] * NUMBER_OF_SUBJECTS
            all_same_answers_df[str(i)] = this_question
    else:
        possible_answers_16_18 = ['1a', '1b']  # list of possible answers for question 16 and 18
        for k in range(1, NUMBER_OF_QUESTIONS + 1):

            if k == 16 or k == 18:
                this_question = np.random.choice(possible_answers_16_18, NUMBER_OF_SUBJECTS)

            else:
                this_question = [value] * NUMBER_OF_SUBJECTS

            all_same_answers_df[str(k)] = this_question


    total_col = list(all_same_answers_df.sum(axis=1))  # this will sum the int values in the rows.
    cat_col = []
    cat_int_col = []

    for i in range(all_same_answers_df.shape[0]):

        str_16 = str(all_same_answers_df.iloc[i, 16])
        str_18 = str(all_same_answers_df.iloc[i, 18])
        total_col[i] += int(''.join(x for x in str_16 if x.isdigit()))
        total_col[i] += int(''.join(x for x in str_18 if x.isdigit()))

        if 0 <= total_col[i] <= 9:
            cat_col.append('MINIMAL')
            cat_int_col.append(0)
        elif 10 <= total_col[i] <= 18:
            cat_col.append('MILD')
            cat_int_col.append(1)
        elif 19 <= total_col[i] <= 29:
            cat_col.append('MODERATE')
            cat_int_col.append(2)
        elif 30 <= total_col[i] <= 63:
            cat_col.append('SEVERE')
            cat_int_col.append(3)

    all_same_answers_df['total'] = total_col
    all_same_answers_df['category'] = cat_col
    all_same_answers_df['category_int'] = cat_int_col


    if all_same_answers_path is not None:
        all_same_answers_df.to_csv(all_same_answers_path, index=None)

    return all_same_answers_df




def combine_per_question_match_counts(EWdata_per_question_match, erisk_per_question_match):
    """
    Combining the two datasets to see how many exact matches we have for each question over all the data.
    :param EWdata_per_question_match: csv file with the per-question match count in the EWdata dataset
    :param erisk_per_question_match: csv file with the per-question match count in the erisk dataset
    :return: None
    """
    EWdata_per_question_match = EWdata_per_question_match.sort_values(by=["Question Number"])
    erisk_per_question_match = erisk_per_question_match.sort_values(by=['Question Number'])

    combined = pd.merge(EWdata_per_question_match, erisk_per_question_match, on='Question Number')
    combined['Combined Match count out of 250'] = combined['Match Count out of 230'] + combined['Match Count out of 20']
    combined['Combined match percentage out of 250'] =100 * (combined['Combined Match count out of 250'] / 250)

    combined = combined.sort_values(by=['Combined Match count out of 250'], ascending=False)


    combined.to_csv(os.path.join(TASK_3_DATA_PATH, "results/per_question_match_count_erisk_EWdata.csv"), index=None)



if __name__ == '__main__':

    """
    supervised_predictions_file_name = "Leon_first_run_holdout.predictions.csv"

    predictions_csv_file = add_score_and_category(supervised_predictions_file_name)
    actual_file = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "EWdata_processed.csv"))

    predictions_csv_file_name = supervised_predictions_file_name.split(".csv")[0] + "_processed.csv"
    compare_per_question_and_category_on_holdout(predictions_csv_file_name, actual_file)
    """


    if ERISK:
        ground_truth_txt_path = os.path.join(TASK_3_DATA_PATH, "results/T3_erisk_golden_truth.txt")
        predictions_txt_path = os.path.join(TASK_3_DATA_PATH, "results/Task3_predictions_GPT_erisk_results_nearest_unsupervised.txt")
        #predictions_txt_path = os.path.join(TASK_3_DATA_PATH,"results/GPT_erisk_results.txt")

    else:
        ground_truth_txt_path = os.path.join(TASK_3_DATA_PATH, "results/EWdata_actual_lables.txt")
        predictions_txt_path = os.path.join(TASK_3_DATA_PATH, "results/GPT_EWdata_results_nearest.txt")


    new_ground_truth_txt_path = convert_tab_delimiter_to_space(ground_truth_txt_path)

    ground_truth_csv_path = convert_txt_to_csv_and_process(new_ground_truth_txt_path)
    predictions_csv_path = convert_txt_to_csv_and_process(predictions_txt_path)


    ground_truth_csv_file = pd.read_csv(ground_truth_csv_path)
    predictions_csv_file = pd.read_csv(predictions_csv_path)


    value = 0

    if value == 0:
        all_same_answers_path = os.path.join(TASK_3_DATA_PATH, "results/same_answers_" + str(value) + ".csv")
        all_same_answers_df = generate_all_same_answers(ground_truth_csv_file, value, all_same_answers_path)
        predictions_csv_file = all_same_answers_df

    else:
        random_results_path = os.path.join(TASK_3_DATA_PATH, "results/random_all_answers_" + str(value) +"_results.csv")
        compare_ground_truth_and_random_answers(ground_truth_csv_file, random_results_path, value)



    AHR_res, ACR_res, ADODL_res, DCHR_res = compare_ground_truth_and_predictions(ground_truth_csv_file, predictions_csv_file)
    print ("AHR: ", AHR_res)
    print ("ACR: ", ACR_res)
    print ("ADODL: ", ADODL_res)
    print ("DCHR: ", DCHR_res)

    """
    # i'm giving it path and not the file because I want to use the path name.
    match_per_question_analysis(ground_truth_csv_path, predictions_csv_path)


    random_results_path = os.path.join(TASK_3_DATA_PATH, "results/random_answers_results.csv")
    compare_ground_truth_and_random_answers(ground_truth_csv_file, random_results_path)

    
    EWdata_per_question_match_count_path = os.path.join(TASK_3_DATA_PATH ,
                                                        "results/GPT_EWdata_results_nearest_per_question_match_count.csv")
    erisk_per_question_match_count_path = os.path.join(TASK_3_DATA_PATH ,
                                                       "results/Task3_predictions_GPT_erisk_results_nearest_unsupervised_per_question_match_count.csv")
    EWdata_per_question_match_count = pd.read_csv(EWdata_per_question_match_count_path, index_col=None)
    erisk_per_question_match_count = pd.read_csv(erisk_per_question_match_count_path, index_col=None)

    combine_per_question_match_counts(EWdata_per_question_match_count, erisk_per_question_match_count)
    
    """
    


