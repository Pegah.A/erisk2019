
import pandas as pd
import spacy
import os


PROCESSED_PATH  = "/Users/pegah_abed/Documents/ERISK/data/T3_data/organized"


def generate_sentence_df(df, parsed_text_col, post_id_col):
    corpus_dict = {}
    for i, doc in df.iterrows():
        document_dict = {}
        post_id = doc[post_id_col]
        parsed_doc = doc[parsed_text_col]
        for i, sentence in enumerate(parsed_doc.sents):
            document_dict[i] = str(sentence)
        corpus_dict[post_id] = document_dict
    
    sentences = pd.DataFrame(corpus_dict).T.reset_index().rename(columns={'index': 'post_id'})
    sentences = pd.melt(sentences, id_vars='post_id', var_name='sentence_num', value_name='body')
    sentences = sentences.dropna().sort_values(['post_id', 'sentence_num'])
    
    return sentences


if __name__ == '__main__':

    df = pd.read_csv(os.path.join(PROCESSED_PATH,'EWdata_processed.csv'))

    
    nlp = spacy.load('en')
    df['all_text_parsed'] = df['Text'].astype(str).apply(nlp)
    all_text_sentences = generate_sentence_df(df, 'all_text_parsed', 'ID')
    all_text_sentences.body = all_text_sentences.body.str.strip()

    OUTPUT_PATH = os.path.join(PROCESSED_PATH , 'EWdata_sentences.csv')
    print('Writing all text sentences (shape: {} to {}'.format(all_text_sentences.shape,OUTPUT_PATH))
    all_text_sentences.to_csv(OUTPUT_PATH, index=None)
