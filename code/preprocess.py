import pandas as pd
import os
import xml.etree.ElementTree as et
import glob
import numpy as np


NUM_USERS = 20
CURRENT_PATH = "/Users/pegah_abed/Documents/"
TASK_1_DATA_PATH = ""
TASK_3_DATA_PATH = os.path.join(CURRENT_PATH,"ERISK/data/T3_data/organized")

NUMBER_OF_CHUNKS = 100


def flatten_cols(df):
    df.columns = [
        '_'.join(tuple(map(str, t))).rstrip('_')
        for t in df.columns.values
    ]
    return df

def generate_erisk_data_csv_file():
    """
    Converts the subject xml files into csv and concatenates all the csv files into a single csv file.
    :return: None
    """

    task_3_csv_path  = os.path.join(TASK_3_DATA_PATH, "erisk_task_3.csv")

    column_names = ['user_id', 'post_id', 'title', 'date', 'info', 'text']
    task_3_csv = pd.DataFrame(columns= column_names)

    user_files = list(glob.glob(os.path.join(TASK_3_DATA_PATH ,'*.xml')))

    for user_file in user_files:
        tree = et.parse(user_file)
        root = tree.getroot()
        user_id = root.find('ID').text
        posts = root.findall('WRITING')

        all_posts = dict((k,[]) for k in column_names) # create one dictionary per user

        post_id_count = 0  # to start counting this user's posts
        for post in posts:

            # keep adding the info of each of the user's text into the dictionary
            all_posts['user_id'].append(user_id)
            all_posts['title'].append(post.find('TITLE').text)
            all_posts['post_id'].append(post_id_count)
            all_posts['date'].append(post.find('DATE').text)
            all_posts['info'].append(post.find('INFO').text)
            all_posts['text'].append(post.find('TEXT').text)

            post_id_count += 1

        subject_df = pd.DataFrame.from_dict(all_posts) # convert user dict to dataframe
        task_3_csv = task_3_csv.append(subject_df, ignore_index=True) # appending the dataframe to the main csv file

    task_3_csv = task_3_csv[column_names] # to preserve the order of the columns
    task_3_csv.to_csv(task_3_csv_path)


def aggregate_features_per_post(sentence_file_name, post_file_name):
    """
    Uses the sentence_file_name to read the sentence-level features file. Aggregates the feature vectors to post-level.
    Saves the post-level feature vectors into a file using the post_file_name.

    :param sentence_file_name: String. The sentence-level features csv file name.
    :param post_file_name: String. The post-level features csv file name.
    :return: None
    """

    GPT_features_per_post_path = os.path.join(TASK_3_DATA_PATH, post_file_name)
    GPT_features_per_sentence = pd.read_csv(os.path.join(TASK_3_DATA_PATH, sentence_file_name), index_col=0)

    GPT_features_per_sentence = GPT_features_per_sentence.drop(columns=['sentence_num', 'body'])

    GPT_features_per_post = flatten_cols(GPT_features_per_sentence.groupby(['user_id','post_id']).agg(['mean']))
    GPT_features_per_post.to_csv(GPT_features_per_post_path)


def aggregate_features_per_user(post_file_name, user_file_name):
    """
    Uses the post_file_name to read the post-level features file. Aggregates the feature vectors to user-level.
    Saves the user-level feature vectors into a file using the user_file_name.

    :param sentence_file_name: String. The post-level features csv file name.
    :param post_file_name: String. The user-level features csv file name.
    :return: None
    """

    GPT_features_per_user_path = os.path.join(TASK_3_DATA_PATH, user_file_name)
    GPT_features_per_post = pd.read_csv(os.path.join(TASK_3_DATA_PATH, post_file_name), index_col=0)

    GPT_features_per_post = GPT_features_per_post.drop(columns=['post_id'])

    GPT_features_per_user = flatten_cols(GPT_features_per_post.groupby(['user_id']).agg(['mean']))
    GPT_features_per_user.to_csv(GPT_features_per_user_path)


def add_user_id_to_EWdata_sentence_features(file_name):
    """
    In the EWdata, there is only one post per user. Threfore, in the sentence_features csv file we only have 'post_id' column.
    In this function, I add a new column 'user_id' which has the same values as the 'post_id' column in the same csv file and save it.
    This function should only be run once.

    :param file_name: String. The name of the EWdata sentence-level features file.
    :return: None
    """

    EWdata_sentence_features = pd.read_csv(os.path.join(TASK_3_DATA_PATH, file_name), index_col=0)

    post_id_col = list(EWdata_sentence_features['post_id'])
    EWdata_sentence_features['user_id'] = post_id_col

    cols = EWdata_sentence_features.columns.tolist()
    cols = cols[-1:] + cols[:-1]

    EWdata_sentence_features = EWdata_sentence_features[cols]
    EWdata_sentence_features.to_csv(os.path.join(TASK_3_DATA_PATH, file_name), index=False)



def process_EW_data():
    """
    Reads the EWdata.csv file. Removes the unrelated columns and rows that contain NaN for BDI values.
    Stores the remaining rows and columns into a new csv file: EWdata_processed.csv
    :return: None
    """

    # the utf-8 encoding will not work
    p_data = pd.read_csv(os.path.join(TASK_3_DATA_PATH, "EWdata.csv"), encoding="latin-1")

    cols = [c for c in p_data.columns if 'MAS' not in c]
    p_data = p_data[cols].dropna()
    p_data = p_data.drop(columns='FileID')
    p_data.to_csv(os.path.join(TASK_3_DATA_PATH, "EWdata_processed.csv"), index=False)


def open_and_check(file_name):
    """
    Opens the csv file using the file_name parameter. Prints the head of the file and the list of its columns.
    :param file_name: String. The name of the file that you want to check
    :return: None
    """

    file_to_check = pd.read_csv(os.path.join(TASK_3_DATA_PATH, file_name))

    print (file_to_check.head())
    print (list(file_to_check))



def stat_on_number_of_sentences_per_person(data_file_path):
    """
    This function will show the number of sentences that each user has in their history.
    This can be useful if you want to split user sentences into chunks to average over.
    It can help you decide how many chunks to have or how many sentences to put in each chunk.
    :param data_file_path: str. path to the data file that contains sentence-level info on the users/
    :return: None
    """

    data_file = pd.read_csv(data_file_path)

    print (list(data_file))
    data_file = data_file[['user_id']]
    res = list(data_file['user_id'].value_counts())

    print (res)
    print (min(res)) # the minimum number of sentences in a user's writing history
    print (max(res)) # the maximum number of sentences in a user's writing history



def split_sentences_into_chunks(all_sentence_features_path, number_of_chunks, all_user_sentences_in_chunk_path):
    """
    This function takes all the sentence-level features of each user and the number of chunks
    and will split the sentences into those number of chunks. The sentences within each chunk will be aggregated (mean)
    to generate a single feature vector. At the end, we will have number_of_chunks feature vectors per user.

    Notes: Here, we are splitting the sentences of each user into fixed number of chunks.
    For example, if the number_of_chunks is 10, user A with 100 sentences and user B with 1000 sentences will both have 10 feature vectors at the end.
    The only difference is that for user A, each feature vector is the average over 10 sentences (10/number_of_chunks) whereas for user B
    each feature vector is the average over 100 sentences (1000/number_of_chunks).

    :param all_sentence_features_path: str. path to the file which contains the sentence level features.
    :param number_of_chunks: int. Determines how many feature vectors we want per user. Each chunk of sentences will provide one feature vector.
    :param all_user_sentences_in_chunk_path: str. Path for saving the chunk feature vectors.
    :return: None
    """

    all_sentence_features = pd.read_csv(all_sentence_features_path, index_col=None)
    user_ID_list = set(all_sentence_features['user_id'])

    all_sentence_features = all_sentence_features.drop(columns=['post_id', 'sentence_num', 'body'])
    all_sentence_features_grouped = all_sentence_features.groupby('user_id')

    # I will first create a data frame with the feature columns and add the user_id column later.
    final_column_names = list(all_sentence_features)[1:]
    all_user_sentences_in_chunks_df = pd.DataFrame(columns=final_column_names)

    for ID in user_ID_list:
        print ("ID is :", ID)

        # drop redundant columns (any better way of NOT creating them in the first place?)
        this_ID_group = all_sentence_features_grouped.get_group(ID).drop('Unnamed: 0', axis=1).reset_index().drop(
            'index', axis=1)
        this_ID_group = this_ID_group.drop(columns=['user_id'])

        # the number of sentences that this user has in their history
        this_ID_sentence_count = len(this_ID_group)
        print ("This ID has {} sentences".format(this_ID_sentence_count))

        # number of sentences that should be in each window to be able to create number_of_chunks chunks out of the user's sentences
        window_size = this_ID_sentence_count // number_of_chunks

        ID_column = [ID] * number_of_chunks

        # performing rolling window and taking the mean of the items within the window
        # [::window_size] works somehow similar to the concept of "stride" in CNNs.
        # it is used here to make sure the windows don't overlap

        this_ID_splitted_df = this_ID_group.rolling(window_size).mean().dropna()[::window_size]
        this_ID_splitted_df = this_ID_splitted_df.iloc[0:number_of_chunks]

        # adding the user_id column
        this_ID_splitted_df['user_id'] = ID_column

        # adding this user's chunks to the general dataframe that contains all the users' chunks.
        all_user_sentences_in_chunks_df = all_user_sentences_in_chunks_df.append(this_ID_splitted_df, ignore_index=True)

    # to maintain the column orders in the saved csv
    all_user_sentences_in_chunks_df = all_user_sentences_in_chunks_df[final_column_names]

    all_user_sentences_in_chunks_df.to_csv(all_user_sentences_in_chunk_path, index=None)



if __name__ == '__main__':


    """
    #generate_erisk_data_csv_file()

    features_source = 'universal'
    data_source = 'EWdata'

    sentence_file_name = features_source + "_" +  data_source + "_" + "sentence_features.csv"
    post_file_name =  features_source + "_" +  data_source + "_" + "post_features.csv"
    user_file_name =  features_source + "_" +  data_source + "_" + "user_features.csv"


    open_and_check(sentence_file_name)
    aggregate_features_per_post(sentence_file_name, post_file_name)
    open_and_check(post_file_name)
    aggregate_features_per_user(post_file_name, user_file_name)
    open_and_check(user_file_name)
    """

    data_file_path = os.path.join(TASK_3_DATA_PATH, "GPT_erisk_sentence_features.csv" )
    #stat_on_number_of_sentences_per_person(data_file_path)


    all_user_sentences_in_chunk_path = os.path.join(TASK_3_DATA_PATH, "GPT_erisk_sentences_in_" + str(NUMBER_OF_CHUNKS) + "_chunks.csv")
    split_sentences_into_chunks(data_file_path, NUMBER_OF_CHUNKS, all_user_sentences_in_chunk_path)





