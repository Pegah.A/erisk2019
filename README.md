**Task** <br />
This repository is for Task 3 of the eRisk 2019 competition. <br />
More details about the competition are here: <br />
http://early.irlab.org <br />
As explained in the above link, the goal is to predict the user's answers to the BDI (Beck's Depression Inventory) questionnaire based on their writing history.<br /><br />

**Data Sets** <br />
We have two sets of data: <br />
The first set is the data provided by the eRisk. This is the data of 20 users and the history of their writing. There is an xml file for each user. We can convert and concatenate all these files into a single csv file. This csv file will have the following columns: post_id, user_id, title, date, info, text. <br />
The second set is the data gathered by a PhD student on journal entries of several students and theire responses tp the BDI questionnaire. This data set includes 230 users. <br />
For privacy reasons, I cannot share any of the actual writings here.<br /><br />

**Feature Generation Tools**<br />
We have used GPT and Universal Sentences to generate features for the user texts. <br />
One option to use these features for predicting users' answers based on their writing history is to apply unsupervised techniques. <br />
I have computed the cosine similarity of the text feature vectors and BDI answers. <br />For each question, the answer that gives the highest cosine similarity to the user's feature vector is chosen.<br /><br />

**Unsupervised: Using Cosine Similarity**<br />
There are several approaches of using cosine similarity:<br />
For each user's writing history, we define a single feature vector. We will first break the user's writings into sentences and generate features per sentence. Then, we will aggeregate these sentence-level feature vectors to get user-level feature vectors. For aggregation, we use the mean. As a result, the entire writing history of each person can be averaged into a single feature vector. The size of this feature vector depends on the method used to generate the features. GPT and BERT give vectors of size 768. Universal Sentences feature vectors are of size 512. After generating a user-level feature, we will then generate feature vectors for each answer of each of the BDI questions. In total, we will have 90 feature vectors for the BDI questionnaire. Now, for each question, we compute the cosine similarity between the user's feature vector and each of the BDI answer feature vectors that belong to that question. The answer that gives the highest cosine similarity value will be chosen as this question's answer by this user. One disadvantage of this approach is that by averaging all the sentence-level features we might lose important context and information. In particular, if the user has many sentences, representing the long history by only one feature vector seems inaccurate. 


For each user's writing history, we generate a feature vector on a sentence level. Similar to the first approach, we also generate feature vectors for each of the BDI asnwers. The size of theser feature vectors depends on the method used to generate the features. GPT and BERT give vectors of size 768. Universal Sentences feature vectors are of size 512. In order to predict the user's answer to each of questions, we compute the copsine similarity of each of the answers to 'all' of the user's senteces. We pick the answer that has the highest cosine similarity to 'any' of the sentences that this user has ever written.  This approach is more computationally heavy since we have to compute the cosine similarity for each sentence. However, we will capture that context if it has been ever mentioned. In other words, there is no averaging performed which would cause the context of the sentences to get lost. <br />
 On the other hand, this approach makes the predictions very sensitive to every single sentence. Even if the sentence's contenxt has been mentioned only once in the writing history or a long time ago. It should not be any surprise if we see a lot of predictions of 'moderate' or 'severe' depression with this approach. 


Another approach is to do something in between approach 1 and approach 2. We can set a chunk_size and then pick chunk_size number of sentences of each user. Then, we average over only this chunk of sentences to get a single feature vector. <br />  
As anexample, if user A has 1000 sentences in their writing history and chunk_size is 20, then we will have 50 feature vectors that represent user A's writing history. (We group user A's sentences into chunks of 20 and average the sentence-level feature vectors of these 20 sentences. We will end up with 50 chunks and therefore, 50 feature vectors.)<br /><br />
Another way of performing this approach is to have a fixed number_of_chunks value that determines how many chunks we want. The final number of feature vectors for each user will be equal to the number of chunks. <br />
For example, if user A has 1000 sentences and the number_of_chunks is 20, this person's writings will be divided into 20 chunks of size 50. Each chunk contains 50 sentence-level feature vectors which will be aggregated (mean) to create a single feature vector per chunk <br />
If another user, B, has 1200 sentences, they as well, will have 20 feature vectors at the end. In this case, each feature vector is created by averaging 1200/20 = 60 sentences within each chunk. <br /> <br />
**Code** <br />
The code that is provided in this repository performs only approach 1, 2, and 3.2 (fixed number_of_chunks value) <br />
This repository does not include the code for generating features from GPT, BERT, ... <br />
The sentence-level features are assumed to be already available.<br /><br />

**Feature Files** <br />
We have feature files with different levels of granularity: <br />
sentence-level features <br />
post-level features <br />
user-level features <br />The aggragation of sentence-level features to post-level and user-level happens in preprocess.pyEach of these files could be generated from GPT, BERT, Universal Sentences, or any other tool/technique that would generate embeddings / feature vectors.sentence-level feature csv file format:<br /><br />
columns: user-id, post_id, sentence_num, body, features. <br />
768 feature columns if using GPT <br />
768 feature columns if using BERT <br />
512 feature columns if using Universal Sentences <br /><br />

post-level feature csv file format: <br />
columns: user-id, post_id, features <br />
768 feature columns if using GPT <br />
768 feature columns if using BERT <br />
512 feature columns if using Universal Sentences <br /><br />

user-level feature csv file format: <br />
columns: user-id, features <br />
768 feature columns if using GPT <br />
768 feature columns if using BERT <br />
512 feature columns if using Universal Sentences <br /><br /> 

**.py Files** <br />
process_sentences.py <br />
This file contains functions to break the input data of users into sentence level. The output of this file can be used as an input to generate sentence-level feature vectors with GPT, universal Sentences, ... . <br /> <br />
preprocess.py <br />
This file contains functions for pre-processing the data and converting the csv files into certain formats. <br />
There are also function to generate post-level and user-level feature vectors given the sentence-level feature vectors. <br /> <br />
similarity.py <br />
This file contains functions for predicting BDI answers for users based on the cosine similarity between user's wiritng history and BDI answers. <br />
In the main function, you need to indicate your source of feature generation and the data set that you would like to use. <br />
If you use the EWdata data set, you can also compare your predicted answers to the actual answers. <br /> <br />
process_supervised_results.py<br />
This file is to process and enavluate the predicted results on a holdout set from autosklearn. <br />
I will compute the score and category for each user and compare the predicted results with the actual values.<br />

       